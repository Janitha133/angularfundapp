import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/services/events/event.service';
import { ActivatedRoute } from '@angular/router';
import { IEvent, ISession } from 'src/app/models/event.model';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss'],
})
export class EventDetailsComponent implements OnInit {
  event: IEvent | undefined;
  addMode: boolean = false;
  filterBy: string = 'all';
  sortBy: string = 'name';

  constructor(
    private eventService: EventService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.event = this.eventService.getEvent(+this.route.snapshot.params['id']);
  }

  addSession() {
    this.addMode = true;
  }

  saveNewSession(session: ISession) {
    if (this.event) {
      const nextId = Math.max.apply(
        null,
        this.event.sessions.map((s) => s.id)
      );
      session.id = nextId + 1;
      this.event?.sessions.push(session);
      this.eventService.updateEvent(this.event);
      this.addMode = false;
    } else {
      alert('Something wrong!');
    }
  }

  cancelAddSession() {
    this.addMode = false;
  }
}
