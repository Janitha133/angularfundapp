import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { ISession } from 'src/app/models/event.model';

@Component({
  selector: 'app-session-list',
  templateUrl: './session-list.component.html',
  styleUrls: ['./session-list.component.scss'],
})
export class SessionListComponent implements OnInit, OnChanges {
  @Input() sessions: ISession[] | undefined;
  @Input() filterBy: string = '';
  @Input() sortBy: string = '';
  visibleSessions: ISession[] | undefined;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(): void {
    if (this.sessions) {
      this.filterSession(this.filterBy);
      this.sortBy === 'name'
        ? this.visibleSessions?.sort(sortByNameAsc)
        : this.visibleSessions?.sort(sortByVoteDesc);
    }
  }

  filterSession(filter: string) {
    if (filter === 'all') {
      this.visibleSessions = this.sessions?.slice(0);
    } else {
      this.visibleSessions = this.sessions?.filter((session) => {
        return session.level.toLowerCase() === filter;
      });
    }
  }
}

function sortByNameAsc(s1: ISession, s2: ISession) {
  if (s1.name > s2.name) {
    return 1;
  } else if (s1.name === s2.name) {
    return 0;
  } else {
    return -1;
  }
}

function sortByVoteDesc(s1: ISession, s2: ISession) {
  return s2.voters.length - s1.voters.length;
}
