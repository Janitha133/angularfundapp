import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ISession } from 'src/app/models/event.model';
import { restrictedWords } from 'src/app/shared/restricted-words.validators';

@Component({
  selector: 'app-create-session',
  templateUrl: './create-session.component.html',
  styleUrls: ['./create-session.component.scss'],
})
export class CreateSessionComponent implements OnInit {
  @Output() saveNewSession = new EventEmitter<ISession>();
  @Output() cancelAddSession = new EventEmitter<any>();
  sessionForm: FormGroup;

  constructor() {
    this.sessionForm = new FormGroup({
      name: new FormControl('', Validators.required),
      presenter: new FormControl('', Validators.required),
      duration: new FormControl('', Validators.required),
      level: new FormControl('', Validators.required),
      abstract: new FormControl('', [
        Validators.required,
        Validators.maxLength(400),
        restrictedWords(['foo', 'bar']),
      ]),
    });
  }

  ngOnInit(): void {}

  saveSession(sessionValue: any) {
    let session: ISession = {
      id: -1,
      name: sessionValue.name,
      presenter: sessionValue.presenter,
      duration: +sessionValue.duration,
      level: sessionValue.level,
      abstract: sessionValue.abstract,
      voters: [],
    };
    this.saveNewSession.emit(session);
  }

  cancel() {
    this.cancelAddSession.emit();
  }

  get sessionName() {
    return this.sessionForm.controls.name;
  }

  get eventDate() {
    return this.sessionForm.controls.presenter;
  }

  get eventDuration() {
    return this.sessionForm.controls.duration;
  }

  get sessionLevel() {
    return this.sessionForm.controls.level;
  }

  get sessionAbstract() {
    return this.sessionForm.controls.abstract;
  }
}
