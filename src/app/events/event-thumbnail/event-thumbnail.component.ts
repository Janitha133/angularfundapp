import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IEvent } from 'src/app/models/event.model';

@Component({
  selector: 'event-thumbnail',
  templateUrl: './event-thumbnail.component.html',
  styleUrls: ['./event-thumbnail.component.scss'],
})
export class EventThumbnailComponent implements OnInit {
  @Input() event: IEvent | undefined;
  @Output() eventClick = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  getStartTimeClass() {
    // const isEarlyStart = this.event && this.event.time === '8:00 am';
    // return { green: isEarlyStart, bold: isEarlyStart };

    if (this.event && this.event.time === '8:00 am') {
      return ['green', 'bold'];
    }
    return [''];
  }

  getStartTimeStyle() {
    if (this.event && this.event.time === '8:00 am') {
      return { color: '#003300', 'font-weight': 'bold' };
    }
    return {};
  }
}
