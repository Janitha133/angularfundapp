import { TestBed } from '@angular/core/testing';

import { EventListResolver } from './event-list-resolver.service';

describe('EventListResolverService', () => {
  let service: EventListResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventListResolver);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
