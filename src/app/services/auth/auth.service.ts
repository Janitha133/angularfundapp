import { Injectable } from '@angular/core';
import { IUser } from 'src/app/models/user.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  currentUser: IUser | undefined;

  constructor() {}

  loginUser(userName: string, password: string) {
    this.currentUser = {
      id: 1,
      firstName: userName,
      lastName: 'papa',
      userName: 'john',
    };
  }

  updateCurrentUser(firstName: string, lastName: string) {
    if (this.currentUser) {
      this.currentUser.firstName = firstName;
      this.currentUser.lastName = lastName;
    }
  }

  isAuthenticated() {
    return !!this.currentUser;
  }
}
