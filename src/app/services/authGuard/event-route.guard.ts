import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { EventService } from '../events/event.service';

@Injectable({
  providedIn: 'root',
})
export class EventRouteGuard implements CanActivate {
  constructor(private eventService: EventService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot) {
    const eventExixts = !this.eventService.getEvent(+route.params['id']);

    if (eventExixts) {
      this.router.navigate(['/404']);
    }
    return !eventExixts;
  }
}
