import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { EventAppComponent } from './events-app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { EventService } from './services/events/event.service';
import { TOASTR_TOKEN, Toastr } from './services/common/toastr.service';
import { PageNotFoundComponent } from './errors/page-not-found/page-not-found.component';
import { EventRouteGuard } from './services/authGuard/event-route.guard';
import { EventListResolver } from './services/events/event-list-resolver.service';
import {
  EventsListComponent,
  EventDetailsComponent,
  CreateEventComponent,
  EventThumbnailComponent,
  CreateSessionComponent,
  SessionListComponent,
  CollapsibleWellComponent,
} from './events/index';
import { AuthService } from './services/auth/auth.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DurationPipe } from './pipes/duration.pipe';

declare let toastr: Toastr;

@NgModule({
  declarations: [
    EventAppComponent,
    EventsListComponent,
    EventThumbnailComponent,
    NavbarComponent,
    EventDetailsComponent,
    CreateEventComponent,
    PageNotFoundComponent,
    CreateSessionComponent,
    SessionListComponent,
    CollapsibleWellComponent,
    DurationPipe,
  ],
  imports: [BrowserModule, FormsModule, ReactiveFormsModule, AppRoutingModule],
  providers: [
    EventService,
    EventRouteGuard,
    { provide: TOASTR_TOKEN, useValue: toastr },
    { provide: 'canDeactivateCreateEvent', useValue: checkDirtyState },
    EventListResolver,
    AuthService,
  ],
  bootstrap: [EventAppComponent],
})
export class AppModule {}

export function checkDirtyState(component: CreateEventComponent) {
  if (component.isDirty) {
    return window.confirm('Are you sure?');
  }
  return true;
}
