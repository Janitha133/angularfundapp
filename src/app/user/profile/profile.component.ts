import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { TOASTR_TOKEN, Toastr } from 'src/app/services/common/toastr.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  profileForm = new FormGroup({
    firstName: new FormControl(this.authService.currentUser?.firstName, [
      Validators.required,
      Validators.pattern('[a-zA-Z].*'),
    ]),
    lastName: new FormControl(
      this.authService.currentUser?.lastName,
      Validators.required
    ),
  });

  constructor(
    private authService: AuthService,
    private router: Router,
    @Inject(TOASTR_TOKEN) private toastr: Toastr
  ) {}

  ngOnInit(): void {}

  saveProfile(formValues: any) {
    this.authService.updateCurrentUser(
      formValues.firstName,
      formValues.lastName
    );
    // this.router.navigate(['events/']);
    this.toastr.success('Profile Saved!');
  }

  cancel() {
    this.router.navigate(['events/']);
  }

  validateFirstName() {
    let _firstName = this.profileForm.controls.firstName;
    return _firstName.valid || _firstName.untouched;
  }

  validateLastName() {
    let _lastName = this.profileForm.controls.lastName;
    return _lastName.valid || _lastName.untouched;
  }
}
