import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  userName: any;
  password: any;
  mouseoverLogin: any;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {}

  login(formValue: any) {
    this.authService.loginUser(formValue.userName, formValue.password);
    this.router.navigate(['events/']);
  }

  cancel() {
    this.router.navigate(['events/']);
  }
}
